/**
 * Created by iashind on 08.04.15.
 */
var express      = require('express');
var path         = require('path');
var logger       = require('morgan');
var bodyParser   = require('body-parser');
var router       = require('./app/lib/router');

var app = module.exports = express();

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'hbs');
app.set('clientData', {});
app.use(logger('dev'));
app.use(bodyParser.json({limit: '5mb'}));
app.use(bodyParser.urlencoded({ extended: false }));

app.use(express.static(path.join(__dirname, 'public')));
app.use(router);


app.listen(app.get('port'), function() {
  console.log("App is running on port:" + app.get('port'))
});

//catch 404 and forward to error handler
app.use(function(req, res, next) {
    return res.render('index');
});
