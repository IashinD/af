/**
 * Created by iashind on 08.04.15.
 */
var _ = require('lodash');
var express = require('express');
var DataCtrl = require('../controllers/data');
var router = module.exports = express.Router();

[
    {
        path: '/api/data/:page',
        method: 'get',
        middleware: [DataCtrl.getPage]
    },
    {
        path: '/api/data/:page',
        method: 'post',
        middleware: [DataCtrl.savePage]
    }
]
    .forEach(function(route) {
        router[route.method].apply(router, _.flatten([route.path, route.middleware]));
    });
