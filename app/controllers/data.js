/**
 * Created by iashind on 19.05.15.
 */
var actions = module.exports;
var _ = require('lodash');

actions.getPage = function getPage(req, res) {
    var result = {};
    var clientData = req.app.get('clientData');
    var startIndex = (req.params.page-1)*10;
    for (var i = startIndex; i < startIndex + 10; i++) {
        if(clientData[i] === undefined) continue;
        result[i] = clientData[i];
    }
    res.send(result);
};

actions.savePage = function savePage(req, res) {
    var clientDada = req.app.get('clientData');
    _.assign(clientDada, req.body);
    res.sendStatus(202);
};