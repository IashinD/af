First to clone repository run:

    git clone git@bitbucket.org:IashinD/forms.git

Go to directory of the project:

    cd forms

Then install all dependencies:

    npm install

And start the application:

    npm start

Finally you can test the application at

    http://localhost:8080

