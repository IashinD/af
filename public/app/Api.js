/**
 * Created by iashind on 19.05.15.
 */
(function(ng) {
    ng.module('app').service('Api', ApiService);

    ApiService.$inject = ['$resource'];
    function ApiService($resource) {
        this.$resource = $resource;
    }

    ApiService.prototype.FormData = function() {
        return this.$resource('/api/data/:page');
    };

})(window.angular);