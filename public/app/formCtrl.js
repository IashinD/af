/**
 * Created by iashind on 19.05.15.
 */
(function(ng) {
    ng.module('app').controller('FormCtrl', FormController);

    FormController.$inject = ['$stateParams', 'FormService', '$state', 'Api', '$controller'];
    function FormController($stateParams, FormService) {
        this.inputs = FormService.getInputs($stateParams.page - 1);
    }

})(window.angular);