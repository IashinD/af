/**
 * Created by iashind on 20.05.15.
 */
(function(ng) {
    ng.module('app').controller('HomeCtrl', HomeController);

    HomeController.$inject = ['$state', 'Api'];
    function HomeController($state, Api) {
        var ctrl = this;
        this.$state = $state;
        this.FormData = Api.FormData();
        this.currentPage = $state.params.page;
        this.data = this.FormData.get({page: this.currentPage});

        this.allowBack = function() {
            return ctrl.currentPage > 1;
        };
        this.allowNext = function() {
            return ctrl.currentPage < 10;
        };
        this.progressOpts = {
            current: function() {
                return ctrl.currentPage * 10;
            },
            msg: function() {
                return 'Page ' + ctrl.currentPage + ' / 10'
            }
        };
    }

    HomeController.prototype.goNext = function next() {
        this.FormData.save({page: this.currentPage}, this.data);
        this.$state.go('home.form', {page: ++this.currentPage});
    };

    HomeController.prototype.goBack = function next() {
        this.FormData.save({page: this.currentPage}, this.data);
        this.$state.go('home.form', {page: --this.currentPage});
    };


})(window.angular);