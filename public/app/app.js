/**
 * Created by iashind on 19.05.15.
 */
(function(ng) {
    var App = ng.module('app', ['ui.router', 'ngAnimate', 'ngResource']);

    App.config(Config);
    Config.$inject = ['$stateProvider', '$urlRouterProvider', '$locationProvider'];
    function Config($stateProvider, $urlRouterProvider, $locationProvider) {

        $stateProvider
            .state('home', {

                templateUrl: 'app/home.html',
                controller: 'HomeCtrl',
                controllerAs: 'HomeCtrl'
            })
            .state('home.form', {
                url: '/form/:page',

                templateUrl: 'app/form.html',
                controller: 'FormCtrl',
                controllerAs: 'FormCtrl'
            });

        $urlRouterProvider.otherwise('/form/1');
        $locationProvider.html5Mode({
            enabled: true
        });

    }
})(window.angular);