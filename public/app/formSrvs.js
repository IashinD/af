/**
 * Created by iashind on 19.05.15.
 */
(function(ng) {
    ng.module('app').factory('FormService', FormService);

    FormService.$inject = [];
    function FormService() {
        var form = [];
        for(var i = 0; i < 100; i++) {
            form.push({
                id: i,
                type: 'text',
                placeholder: 'input ' + i
            });
        }

        return {
            getInputs: function(page) {
                return form.slice(page*10, page*10 + 10);
            }
        };
    }
})(window.angular);